<?php $colors=array("teal","green","brown","blue" );  $colors_s=array("009688","4CAF50","795548","2196F3" ); $colorss=array_rand($colors); $color= $colors[$colorss]; $color_s=$colors_s[$colorss]; ?>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Profile :: Naveen Kumar S</title>
	<link rel="stylesheet" href="css/materialize.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<style type="text/css">		
		.card-transparent-teal {
			background-color: #<?=$color_s?>66 !important;
		}
		input.validate:focus:not([type]) + label, input.validate:focus[type="text"] + label, input.validate:focus[type="password"] + label, input.validate:focus[type="email"] + label, input.validate:focus[type="url"] + label, input.validate:focus[type="time"] + label, input.validate:focus[type="date"] + label, input.validate:focus[type="datetime"] + label, input.validate:focus[type="datetime-local"] + label, input.validate:focus[type="tel"] + label, input.validate:focus[type="number"] + label, input.validate:focus[type="search"] + label, textarea.materialize-textarea.validate:focus + label
		{
			color:<?=$color?> !important;
			border-bottom:<?=$color?> !important;
		}
	</style>
</head>

<body>
	<main>
		<div class="bg-wrapper">
		</div>
		<div class="container">
			<div class="row">
				<div class="col s2"></div>
				<div class="col s8">
					<div class="card card-transparent-teal">
						<div class="img-center">
							<img src="images/avatar.jpg" class="img circle">
						</div>
						<div class="card">
							<div class="card-content">
								<div class="row">
									<h3 class="center <?=$color?>-text">Naveen Kumar S</h3>
									<p class="center grey-text text-darken-1">Web Developer / Full Stack Developer / DevOps </p>
								</div>
								<br>
								<table>
									<tr>
										<td  class="center <?=$color?>-text">
											<i class="fa fa-lg fa-facebook"></i>
										</td>
										<td class="center <?=$color?>-text">
											<i class="fa fa-lg fa-linkedin"></i>
										</td>
										<td class="center <?=$color?>-text">
											<i class="fa fa-lg fa-twitter"></i>
										</td>
										<td class="center <?=$color?>-text">
											<i class="fa fa-lg fa-google-plus"></i>
										</td>
										<td class="center <?=$color?>-text">
											<i class="fa fa-lg fa-bitbucket"></i>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col s2"></div>
			</div>
		</div>
	</main>
	<footer class="page-footer <?=$color?>">
		<table class="responsive white-text fa-lg" style="margin-bottom: 10px;width:100%;text-align: center;">
			<tr>
				<td style="cursor: pointer;" onclick="$('#about').modal('open')">About</td>
				<td style="cursor: pointer;" onclick="$('#education').modal('open')">Education</td>
				<td style="cursor: pointer;" onclick="$('#experience').modal('open')">Experience</td>
				<td style="cursor: pointer;" onclick="$('#skills').modal('open')">Skills</td>
				<td style="cursor: pointer;" onclick="$('#projects').modal('open')">My Works</td>
				<td style="cursor: pointer;" onclick="$('#publication').modal('open')">Publications</td>
				<td style="cursor: pointer;" onclick="$('#certification').modal('open')">Certification</td>
				<td style="cursor: pointer;" onclick="$('#contact').modal('open')">Contact</td>
			</tr>
		</table>
		<!-- <div class="row">
			<a href="#about" class=""><div class="col s1 btn <?=$color?>">About</div></a>
			<a href="#education" class=""><div class="col s1 btn <?=$color?>">Education</div></a>
			<a href="#experience" class=""><div class="col s1 btn <?=$color?>">Experience</div></a>
			<a href="#skills" class=""><div class="col s1 btn <?=$color?>">Skills</div></a>
			<a href="#projects" class=""><div class="col s1 btn <?=$color?>">My Works</div></a>
			<a href="#contact" class=""><div class="col s1 btn <?=$color?>">Publications</div></a>
			<a href="#contact" class=""><div class="col s1 btn <?=$color?>">Certification</div></a>
			<a href="#contact" class=""><div class="col s1 btn <?=$color?>">Contact</div></a>
		</div> -->



		<!-- <div class="container">
			<nav>
				<div class="nav-wrapper <?=$color?>">
					<a href="#" class="center brand-logo">R</a>
					<ul class="left hide-on-med-and-down" id="nav-mobile">
						<li><a href="#">About</a></li>
						<li><a href="#">Experience</a></li>
						<li><a href="#">Education</a></li>
						<li><a href="#">Skills</a></li>
					</ul>
				</div>
			</nav>
		</div> -->
		
	</footer>
	<div class="modal modal-fixed-footer" id="about">
		<div class="modal-header <?=$color?> white-text">
			<div class="row">
				<div class="col s11">
					<h4 class="center">About</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<p>Bunch of Text</p>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="education">
		<div class="modal-header <?=$color?> white-text">
			<div class="row">
				<div class="col s11">
					<h4 class="center">Education</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<table class="table responsive">
				<tr><th>Degree</th><th>Specialization</th><th>Institution</th><th>Year of Passing</th><th>Percentage</th></tr>
				<tr><td>B.E</td><td>Computer Science &amp; Engineering</td><td>Adithya Institute Of Technology</td><td>2016</td><td>75%</td></tr>
				<tr><td>HSC</td><td>Computer Science</td><td>Nagamani Ammal Memorial Matric Hr School</td><td>2012</td><td>81%</td></tr>
				<tr><td>SSLC</td><td>Matriculation</td><td>Nagamani Ammal Memorial Matric Hr School</td><td>2010</td><td>72%</td></tr>
			</table>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="experience">
		<div class="modal-header <?=$color?> white-text">
			<div class="row">
				<div class="col s11">
					<h4 class="center">Experience</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<table class="table responsive">
				<tr><th>Company</th><th>Designation</th><th>Experience</th></tr>
				<tr><td>Adithya Institute Of Technology</td><td>Freelancer</td><td>1 year</td></tr>
				<tr><td>Vishnu Educational Development and Innovation Center (VEDIV)</td><td>Software Programmer</td><td>10 Months</td></tr>
				<tr><td>Anubavam Technologies Pvt. Ltd.</td><td>Jr. Software Developer</td><td>1 month</td></tr>
			</table>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="certification">
		<div class="modal-header <?=$color?> white-text">
			<div class="row">
				<div class="col s11">
					<h4 class="center">Certifications</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<table class="table responsive">
				<tr><th>S.No</th><th>Title</th><th>Certified by</th></tr>
				<tr><td>1</td><td>AWS (Amazon Web Services) Training on “Big Data Fundamentals”</td><td>Amazon</td></tr>
				<tr><td>2</td><td>C++ Programming</td><td>Saylor Academy</td></tr>
				<tr><td>3</td><td>Introducation to Computer Science I</td><td>Saylor Academy</td></tr>
				<tr><td>4</td><td>Big Data Fundamentals</td><td>Big Data University (IBM)</td></tr>
				<tr><td>5</td><td>Developing Distributed Applications Using ZooKeeper</td><td>Big Data University (IBM)</td></tr>
				<tr><td>6</td><td>Introduction to Pig</td><td>Big Data University (IBM)</td></tr>
				<tr><td>7</td><td>Using BigSheets for Spreedsheet-like Analytics</td><td>Big Data University(IBM)</td></tr>
				<tr><td>8</td><td>Introduction to Jaql</td><td>Big Data University(IBM)</td></tr>
			</table>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="skills">
		<div class="modal-header <?=$color?> white-text">
			<div class="row">
				<div class="col s11">
					<h4 class="center">Skills</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<table class="table responsive">
				<tr><th>S.No</th><th>Skill</th><th>Knowledge</th></tr>
				<tr><td>1</td><td>PHP</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:98%"></div></div></td></tr>
				<tr><td>2</td><td>HTML</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:98%"></div></div></td></tr>
				<tr><td>3</td><td>CSS</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:95%"></div></div></td></tr>
				<tr><td>4</td><td>JS</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>5</td><td>JQUERY</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>6</td><td>MYSQL</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:90%"></div></div></td></tr>
				<tr><td>7</td><td>MONGO-DB</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:90%"></div></div></td></tr>
				<tr><td>8</td><td>R</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:80%"></div></div></td></tr>
				<tr><td>9</td><td>C</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:90%"></div></div></td></tr>
				<tr><td>10</td><td>C++</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>11</td><td>JAVA</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:80%"></div></div></td></tr>
				<tr><td>12</td><td>CODE-IGNITER</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:95%"></div></div></td></tr>
				<tr><td>13</td><td>LARAVEL</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>14</td><td>Yii</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:65%"></div></div></td></tr>
				<tr><td>15</td><td>VISUAL-STUDIO</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:75%"></div></div></td></tr>
				<tr><td>16</td><td>MOODLE</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:95%"></div></div></td></tr>
				<tr><td>17</td><td>OPEN-EDX</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:65%"></div></div></td></tr>
				<tr><td>18</td><td>WORDPRESS</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:80%"></div></div></td></tr>
				<tr><td>19</td><td>JOOMLA</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:70%"></div></div></td></tr>
				<tr><td>20</td><td>DOCKER</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>21</td><td>ANGULAR JS</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:75%"></div></div></td></tr>
				<tr><td>22</td><td>NODE JS</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:75%"></div></div></td></tr>
				<tr><td>23</td><td>SERVER HANDLING</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:85%"></div></div></td></tr>
				<tr><td>24</td><td>SHELL SCRIPTING</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:90%"></div></div></td></tr>
				<tr><td>25</td><td>PYTHON</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:60%"></div></div></td></tr>
				<tr><td>26</td><td>R</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:80%"></div></div></td></tr>
				<tr><td>26</td><td>R</td><td><div class="progress"><div class="determinate <?=$color?>" style="width:80%"></div></div></td></tr>
			</table>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="projects">
		<div class="modal-header <?=$color?> white-text" style="">
			<div class="row">
				<div class="col s11">
					<h4 class="center">My Works</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<div class="col s12">
				<ul class="collapsible popout" data-collapsible="accordion">
					<li class="">
						<div class="collapsible-header"><i class="fa fa-plus"></i>eLab</div>
						<div class="collapsible-body" style="display: none;"><p>eLab is a software used to compile the programs on-line, it decrease the workload of the client system. This software has user and course management with auto evaluation system. It improves the programming and logical skill of the users. It also has a Plagiarism detector, to detect malpractice in programs. It supports more than 18 programming languages</p></div>
					</li>
					<li class="">
						<div class="collapsible-header"><i class="fa fa-plus"></i>eLearn</div>
						<div class="collapsible-body" style="display: none;"><p>eLearn is a learning management system (LMS) is a software application for the administration, documentation, tracking, reporting and delivery of educational courses or training programs. They help the instructor deliver material to the students, administer tests and other assignments, track student progress, and manage record-keeping. LMSs support a range of uses, from supporting classes that meet in physical classrooms to acting as a platform for fully online courses, as well as several hybrid forms, such as blended learning and flipped classrooms.</p></div>
					</li>
					<li class="">
						<div class="collapsible-header"><i class="fa fa-plus"></i>Analyzer</div>
						<div class="collapsible-body" style="display: none;"><p>“ANALYZER” is a tool to analyze the product demand and fixing alternate price for retail sector with php.</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Hospital Management System</div>
						<div class="collapsible-body" style=""><p>“HMS” is a software used to manage the doctor, patient details and history in the database, this software is done in both Visual Studio (VB .NET) and in JSP (NET BEANS).</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Learn c</div>
						<div class="collapsible-body" style=""><p>Learn c is an online learning for C Programming, this allows the users to create a program for a set of questions compile, run and evaluate the programs online.</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Face Detection</div>
						<div class="collapsible-body" style=""><p>Face detection is an android project which authenticate the user with the help of edge detection algorithm.</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Code Here</div>
						<div class="collapsible-body" style=""><p>Code here is an improved model of the Learn C project where code here allows the user to execute C, C++ and java.</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Web Evaluator</div>
						<div class="collapsible-body" style=""><p>Web evaluator is a project where web develepment languages such as html, css , js ,php can be coded online and will be automatically evaluated.</p></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="fa fa-plus"></i>Helper Heart</div>
						<div class="collapsible-body" style=""><p>Helpers heart is a social web application where the user can share their information like, command and chat among the helpers heart users.</p></div>
					</li>
				</ul>    
			</div>
		</div>
	</div>
	<div class="modal modal-fixed-footer" id="publication">
		<div class="modal-header <?=$color?> white-text" style="">
			<div class="row">
				<div class="col s11">
					<h4 class="center">Publications</h4>
				</div>
				<div class="col s1">
					<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
				</div>
			</div>
		</div>
		<div class="modal-content">
			<div class="col s12">
				<ul class="collapsible popout" data-collapsible="accordion">
					<li class="">
						<div class="collapsible-header"><i class="fa fa-plus"></i>Enhancing Programming and Logical Skills Through e-Learning Platform (eLab): A State of Art</div>
						<div class="collapsible-body" style="display: none;">
							<h6><b>Educational Technology Research and Development - Springer</b></h6>
							<p>Under Review</p></div>
						</li>
						<li class="">
							<div class="collapsible-header"><i class="fa fa-plus"></i>Market Basket Analysis using Frequent Itemset</div>
							<div class="collapsible-body" style="display: none;">
								<h6><b>National Conference on Innovative Intelligence in Computer Technology (NCICT 2015)</b></h6>
								<p>In retail markets, Market basket analysis is an important component to determine the placement of goods, designing sales promotions for different segments of customers to improve customer satisfaction and hence the profit of the supermarket. The issues in a leading supermarket are addressed here using frequent itemset mining. The frequent itemsets are mined from the market basket database using the efficient K-Apriori algorithm with the help of mining tools like R. Association rules are separately generated to satisfy customers specific needs in a cost effective manner and also to improve the market sales. It will be useful for the retail sectors to achieve their market goal. In this paper, we have briefly explained how to find the frequently moving items in a market. </p></div>
							</li>
							<li class="">
								<div class="collapsible-header"><i class="fa fa-plus"></i>Different Simulator for Underwater Wireless Technology</div>
								<div class="collapsible-body" style="display: none;">
									<h6><b>International Conference on Innovation in Information, Embedded and Communication Systems</b></h6>
									<p>Underwater sensor networks became an important area of research and its applications towards social issues like global warming, Co2 gas monitoring, geological carbon capture and Storage (CCS) technique consists of capturing Co2 from power and industrial activities and storing it in deep water in order to prevent large quantities of Co2 from being released into the atmosphere, under water oil and gas extraction and distribution. To monitor different parameters in above applications, we need to choose appropriate simulators and there is need of comparison in different simulators. This paper gives you different simulators that were found by Sapienza University for under water sensor networks like SUNSET, SUNSET-2 DESERT and other simulators like WOSS, SENSES, NS2 Miracle, AquaSim Aqua Net, and nautilus. </p></div>
								</li>

							</ul>    
						</div>
					</div>
				</div>
				<div class="modal modal-fixed-footer" id="contact">
					<div class="modal-header <?=$color?> white-text">
						<div class="row">
							<div class="col s11">
								<h4 class="center">Contact</h4>
							</div>
							<div class="col s1">
								<a href="#!" class="modal-action modal-close btn-flat <?=$color?> white-text"><i class="fa fa-lg fa-times"></i></a>
							</div>
						</div>
					</div>
					<div class="modal-content">
						
						<div class="row">
							<form class="col s12">
								<div class="row">
								<div class="input-field col s12">
										<input id="first_name" type="text" class="validate">
										<label for="first_name">Name</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<input id="Subject" type="text" class="validate">
										<label for="Subject">Subject</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<input id="email" type="email" class="validate">
										<label  for="email" data-error="wrong" data-success="right">Email</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<textarea id="Message" class="materialize-textarea" type="text" ></textarea>
										<label  for="Message">Message</label>
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$(".modal").modal();
					});
				</script>
			</body>
			</html>